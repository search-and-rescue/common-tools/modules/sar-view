#! /usr/bin/env python3

import time
from PyQt5.QtCore import QThread, pyqtSignal

from sarcore.modules.interface_joystick import JoyStick

class JoyStickThread(QThread):
    changeKeyPress = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        self.joystick = JoyStick(self,
                                 interface="/dev/input/js0",
                                 connecting_using_ds4drv=False)

        self.delay = 0.02

    def run(self):
        self.joystick.listen(timeout=60)

    def stop(self):
        self.joystick = None

    def signal_update(self, signal):
        self.changeKeyPress.emit(signal)

