#! /usr/bin/env python3

from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtWidgets import QPushButton, QLabel, QSlider, QMessageBox
from PyQt5.QtGui import QImage, QPixmap, QFont, QIcon
from PyQt5.QtGui import QPainter, QPen, QColor
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal, QPoint, QSize, QLine
import qtawesome as qta

from sarview.gui.core_thread import Thread
from sarview.gui.joystick_thread import JoyStickThread

class cameraWindow(QWidget):
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.core = core

        #self.left = 0
        #self.top = 0
        #self.width = self.options.screen_width
        #self.height = self.options.screen_height
        #self.setGeometry(self.left, self.top, self.width, self.height)

        #if not self.options.running_on_arm:
        #    self.setFixedWidth(self.width)
        #    self.setFixedHeight(self.height)

        self.signal_dispatch = {
                'x_press': self.button_x_press,
                'square_press': self.button_square_press,
                }

        self.setUI()

        self.thread = Thread(self, self.options, self.core)
        self.joystick_thread = JoyStickThread()

        self.setStyle()
        self.setLayout()

        self.setWindowFlag(Qt.FramelessWindowHint)

    def setUI(self):
        self.title = f'{self.options.app_name} v{self.options.app_version}'
        self.setWindowTitle(self.title)

        self.image = QLabel(self)

        self.buttonMenu = QPushButton('□', self)
        self.buttonMenu.clicked.connect(self.backToMenu)

        self.labelTitle = QLabel(self)

    def setStyle(self):
        # self.image.resize(self.width, self.height)

        self.buttonMenu.setStyleSheet("""
                background-color: red;
                font-size: 20px;
                padding-left: 5px; padding-right: 5px;
                padding-top: 1px; padding-bottom: 1px;
                border-radius: 6px;
                """)

        self.labelTitle.setStyleSheet('color: white')

    def setLayout(self):
        self.width = self.size().width()
        self.height = self.size().height()
        self.thread.changeWinSize(self.size())

        #Layout Image
        self.image.resize(self.width, self.height)

        #Layout buttonMenu
        y = self.size().height() - self.buttonMenu.size().height()
        p = QPoint(5, y)
        self.buttonMenu.move(p)


        #Layout labelTitle
        if self.options.display_title:
            title = f'{self.options.app_name} v{self.options.app_version}'
        else:
            title = ''

        self.labelTitle.setText(title)

        x = self.size().width()//2 - self.labelTitle.size().width()//2
        p = QPoint(x, 7)
        self.labelTitle.move(p)

    def resizeEvent(self, p):
        self.setLayout()

    def showEvent(self, event):
        self.setStyle()
        self.setLayout()
        self.startCapture()
        #self.show()

    @pyqtSlot(QImage)
    def setImage(self, image):
        self.image.setPixmap(QPixmap.fromImage(image))

    def signalProcess(self, signal):
        fc = self.signal_dispatch.get(signal)
        if fc is not None:
            fc()

    def startCapture(self):
        self.thread.changePixmap.connect(self.setImage)
        self.thread.start()

    def backToMenu(self):
        self.hide()

        self.thread.working = False

        self.close()

    def closeEvent(self, event):
        self.thread.stop()
        event.accept()

    def button_square_press(self):
        self.backToMenu()

    def button_x_press(self):
        pass

