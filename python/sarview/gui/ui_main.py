#! /usr/bin/env python3

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt5.QtWidgets import QPushButton, QLabel
from PyQt5.QtWidgets import QSizePolicy
from PyQt5.QtCore import Qt, pyqtSlot, pyqtSignal, QPoint, QSize, QLine

from sarview.gui.ui_camera import cameraWindow
from sarview.gui.joystick_thread import JoyStickThread

from sarview.gui.ui_tools import Color

class startWindow(QMainWindow):
    def __init__(self, options, core):
        super().__init__()
        self.options = options
        self.core = core

        self.cameraWindow = None

        self.options.camera_updated = True

        self.signal_dispatch = {
                'x_press': self.button_x_press,
                'triangle_press': self.button_triangle_press,
                }

        self.setUI()
        self.setStyle()
        self.setLayout()

        self.joystick_thread = JoyStickThread()
        self.joystick_thread.changeKeyPress.connect(self.signalProcess)
        self.joystick_thread.start()

        self.setWindowFlag(Qt.FramelessWindowHint)

    def setUI(self):
        #self.setFixedWidth(self.options.screen_width)
        #self.setFixedHeight(self.options.screen_height)

        self.label = QLabel('SaR-View')
        self.labelStart = QLabel('Start')
        self.labelQuit = QLabel('Quit')

        self.button_X = QPushButton('X', self)
        self.button_X.clicked.connect(self.startCamera)

        self.button_triangle = QPushButton('▲', self)
        self.button_triangle.clicked.connect(self.button_triangle_press)

    def setStyle(self):
        self.label.setStyleSheet("""
                background-color: black;
                border: 1px solid black;
                font-size:60px; color:grey
                """)

        self.labelStart.setStyleSheet("""
                background-color: black;
                border: 1px solid black;
                font-size:30px; color:grey
                """)

        self.labelQuit.setStyleSheet("""
                background-color: black;
                border: 1px solid black;
                font-size:30px; color:grey
                """)

        self.button_triangle.setGeometry(0,0,50,50)
        self.button_triangle.setStyleSheet("""
                background-color: green;
                color: white;
                font-size: 35px;
                border-radius: 25;
                vertical-align: 5px;
                """)
        self.button_X.setGeometry(0,0,50,50)
        self.button_X.setStyleSheet("""
                background-color: blue;
                color: white;
                font-size: 30px;
                border-radius: 25;
                """)

    def setLayout(self):
        layoutV = QVBoxLayout()
        layoutH = QHBoxLayout()

        layoutV.addWidget(self.label)
        layoutV.addWidget(Color('black'))
        layoutV.addWidget(Color('black'))
        self.button_X.setFixedHeight(50)
        self.button_X.setFixedWidth(50)
        self.button_triangle.setFixedHeight(50)
        self.button_triangle.setFixedWidth(50)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.labelQuit, alignment=Qt.AlignRight)
        layoutH.addWidget(self.button_triangle)
        layoutV.addLayout(layoutH)

        layoutH = QHBoxLayout()
        layoutH.addWidget(self.labelStart, alignment=Qt.AlignRight)
        layoutH.addWidget(self.button_X)
        layoutV.addLayout(layoutH)

        widget = Color('black')
        widget.setLayout(layoutV)
        self.setCentralWidget(widget)


    def showEvent(self, event):
        self.showMaximized()

    def startCamera(self):
        self.core.sensors.discover('camera')

        self.cameraWindow = cameraWindow(self.options, self.core)
        self.cameraWindow.show()

    @pyqtSlot(str)
    def signalProcess(self, signal):
        if self.cameraWindow is not None:
            if self.cameraWindow.isActiveWindow():
                self.cameraWindow.signalProcess(signal)

        if self.isActiveWindow():
            fc = self.signal_dispatch.get(signal)
            if fc is not None:
                fc()

    def button_x_press(self):
        self.startCamera()

    def button_triangle_press(self):
        self.close()

if __name__ == "__main__":
    from types import SimpleNamespace
    options = SimpleNamespace()
    options.screen_width = 800
    options.screen_height = 600

    app = QApplication([])
    start_window = startWindow(options, None)
    start_window.showMaximized()
    app.exit(app.exec_())
