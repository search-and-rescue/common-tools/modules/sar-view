#! /usr/bin/env python3

import time
from PyQt5.QtGui import QImage
from PyQt5.QtCore import QThread, pyqtSignal

class Thread(QThread):
    changePixmap = pyqtSignal(QImage)

    def __init__(self, parent=None, options=None, core=None):
        super().__init__()
        self.options = options
        self.core = core
        self.imageWidth = 640
        self.imageHeight = 480
        self.working = False

        self.delay = 0.02

    def run(self):
        self.working = True

        while self.working:
            start_time = time.time()
            time.sleep(self.delay)

            if self.core.sensors.camera.is_active:
                self.core.sensors.camera.set_frame()

                rgbImage = self.core.sensors.camera.get_rgb_frame()
                h, w, ch = rgbImage.shape
                bytesPerLine = ch * w
                convertToQtFormat = QImage(rgbImage.data, w, h,
                                           bytesPerLine, QImage.Format_RGB888)
                p = convertToQtFormat.scaled(self.imageWidth,
                                             self.imageHeight)
                self.changePixmap.emit(p)


    def stop(self):
        self.working = False

        if self.core.sensors.camera.is_active:
            self.core.sensors.camera.release_capturing_device()

        self.exit(0)

    def changeWinSize(self,size):
        self.imageWidth = size.width()
        self.imageHeight = size.height()
