# !/usr/bin/env python3

import PyQt5.QtWidgets as pyQt

from sarview.gui.ui_main import startWindow
from sarcore.modules.core import SARCore

def main():
    core = SARCore()

    app = pyQt.QApplication([])
    app_win = startWindow(core.options, core)
    app_win.showMaximized()
    app.exit(app.exec_())
